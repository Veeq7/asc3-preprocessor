# Project
ASC3 preprocessor is used to generate starcraft scripting language from more structured and sophisticated syntax. It automates generation of code that you would usually just copy paste billion times, and allows for pseudo-functions. It aims to make AI scripts more easier to read and more complex at the same time. At the end of the day, this preprocessor can be a very powerful code generator that can generate crazy results. (for example Nth dimensional array-like code).

ASC3 preprocessor is supported by PyAI in [PyMS-Veeq](https://gitlab.com/Veeq7/PyMS-Veeq).

[Download](Binaries/Release/x64/ASC3Preprocessor.exe)

# How to use
To compile your scripts you can either:
- Have an `input.asc3` in the same directory as .exe file and run the preprocessor.
- Drag and drop an `.asc3` file onto the preprocessor.
- Use it directly in PyAI from [PyMS-Veeq](https://gitlab.com/Veeq7/PyMS-Veeq).

I strongly recommend writing scripts in [VS Code](https://code.visualstudio.com/) with my [ASC3 extension](https://marketplace.visualstudio.com/items?itemName=veeq7.asc3-language). I believe it's currently the best method to write your scripts, due to it's robust auto completion, command documentation and syntax highlighting. It's also easy to setup. Alternatively you can use [Notepad++](https://notepad-plus-plus.org/) with auto complete and syntax highlighting configs from utils folder in [PyMS-Veeq](https://gitlab.com/Veeq7/PyMS-Veeq).

# Documentation

Currently documentation is quite rough, I have only enlisted keywords and literals without much description. I also included an example script that uses most of the current features (callblock is the same as multiblock, but uses call instead of multirun, hence I didn't include it). Most of the functionality should be easy to learn and intuitive after some experimentation.

## Compiling and debugging
Program is intended to be compiled in Visual Studio 2019. In your scripts you can add `###@` at the end of a line to trigger a breakpoint in visual studio.

## Keywords

Currently supported keywords:
- `macro name args... { block }`<br>
Declares a macro.

- `multiblock name args... { block }`<br>
Declares block at the end of the file and multiruns it. User is meant to include stop at the end of the block.

- `callblock name args... { block }`<br>
Declares block at the end of the file and calls it. User is meant to include return at the end of the block.

- `use varname=varvalue... { block }`<br>
Declares variables for use in it's block.

- `include filepath ...`<br>
Preprocesses and includes another script, filepath can be relative or absolute. In PyAI you should put scripts in Scripts/ folder (or it's subfolder) and specify a relative path from it. More details in that [folder](https://gitlab.com/Veeq7/PyMS-Veeq/-/tree/master/Scripts).

- `if numericExpression { block }`<br>
If numericExpression is true *(0 - false, other numbers - true)*, then evaluate body. numericExpression is automatically interpreted as an expression. Works with numbers.

- `compare str1 operator str2 { block }`<br>
If str1 is equal (or is not) to str2 then do block (`==`- equal, `!=` not equal). Works with strings.

- `range var min max { block }`<br>
Generates a loop using var as iterator ranging between min and max. Can be nested.

- `foreach var array { block }`<br>
Loops trough an array using var. Array items are seperated by a comma fe. `item1,item2,item3`. Can be nested. Additional bindings can be defined for iterating over pairs, triples etc. (fe. `count:unit 4:marine,3:zealot`).

- `indexof array index$key`<br>
Returns value of array at index. $key is optional and can be specified for arrays that use bindings.

- `define name value`<br>
Defines a global. Can be invoked using `%name`.

- `map mapName key value`<br>
Declares a mapping of value within a map using a key. Can be invoked using `@map$key`, using variable inside is allowed, for example `@%mapName$%keyName`.

## Literals

Currently supported literals:
- `!` - executes macro, multiblock or callblock.
- `*` - generates multiblock or callblock, returns it's name instead of usual execution code.
- `=` - evaluates expression or an int variable, supports logical operations, arithmetics and bitwise operations (see [below](#expression-operators)). Expression ends on any whitechar *(fe. newline, space, tab)*.
- `;` - Allows to specify an end point for an expression, useful for things like points (fe. `(=x+10 =y+10;)`)
- `%` - returns value of a variable
- `,` - separates items in array (no spaces allowed)
- `:` - seperates value in array item (no spaces allowed)
- `@map$key` - returns value from `@map` using `$key`.
- `&block` - returns name of current block, only supported for multiblock and callblock
- `&index` - returns current index of highest scope's foreach
- `&size` - returns array size of highest scope's foreach
- `#` - comment that is removed by the preprocessor
- `##` - comment that is not removed by the preprocessor
- `###` - comment that is not removed by the preprocessor, but is also not processed

\*- please note that in-line comments are never removed and always processed by the preprocessor

## Expression Operators
Those can be used in expression with `=` prefix
- `!` - Logical NOT
- `~` - Bitwise NOT
- `*` - Multiplication
- `/` - Division
- `%` - Modulation
- `+` - Addition
- `-` - Subtraction
- `<>` - Logical Not Equal
- `|=` - Bitwise Or Equal
- `&=` - Bitwise And Equal
- `^=` - Bitwise XOR Equal
- `<=` - Logical Less or Equal
- `>=` - Logical Greater or Equal
- `!=` - Logical Not Equal
- `<<` - Bitwise Left Shift
- `>>` - Bitwise Right Shift
- `==` - Logical Equals
- `>` - Logical Greater Than
- `<` - Logical Less Than
- `&&` - Logical AND
- `||` - Logical OR
- `|` - Bitwise OR
- `&` - Bitwise AND
- `^` - Exclusive OR

# Example Script
Input file:<br>
[input.asc3](DEMO/input.asc3)

File included by input file:<br>
[common.asc3](DEMO/common/common.asc3)

Output file:<br>
[output.asc3](DEMO/output.asc3)