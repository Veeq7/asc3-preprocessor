## Task Sheet

### General
- Add support for array random access (eg. @item1,item2,item3$0 -> item1)
- Improve error detection and handling.
- Improve performance if possible.