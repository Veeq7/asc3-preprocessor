#include "string_helper.h"

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <algorithm>

namespace string_helper {
	std::vector<std::string> split(const std::string& str) {
		std::vector<std::string> result;
		std::istringstream iss(str);
		for (std::string s; iss >> s; ) {
			result.push_back(s);
		}
		return result;
	}

	std::vector<std::string> split(const std::string& str, char split) {
		std::vector<std::string> result;
		std::string temp;
		for (char c : str) {
			if (c == split) {
				result.push_back(temp);
				temp = "";
			}
			else {
				temp += c;
			}
		}
		if (temp != "") {
			result.push_back(temp);
		}
		return result;
	}

	std::string ltrim(const std::string& str) {
		std::string s = str;
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
			return !std::isspace(ch);
			}));
		return s;
	}

	std::string rtrim(const std::string& str) {
		std::string s = str;
		s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
			return !std::isspace(ch);
			}).base(), s.end());
		return s;
	}

	std::string trim(const std::string& str) {
		std::string s = ltrim(str);
		return rtrim(s);
	}

	std::string join(std::vector<std::string> arguments) {
		std::string result = "";
		for (std::string arg : arguments) {
			result.append(arg + " ");
		}
		return result.substr(0, result.size() - 1);
	}

	std::string join_file_path(std::vector<std::string> arguments) {
		std::string result = join(arguments);
		std::replace(result.begin(), result.end(), '\\', '/');
		return result;
	}

	void stream_snapshot(std::stringstream& stream, const std::string& outputPath) {
		std::ofstream output(outputPath);
		output << stream.str();
	}

	std::string print_error(const std::string& str) {
		std::string s = "Error: " + str;
		std::cout << s << std::endl;
		return "\t### " + s + "\n";
	}
}
