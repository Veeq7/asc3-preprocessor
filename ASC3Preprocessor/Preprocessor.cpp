#include "preprocessor.h"

#include <unordered_set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>

#include "Commands.h"
#include "string_helper.h"

#if _DEBUG
#include <intrin.h>
#endif

namespace prp {
	void process(std::istream& input, std::ostream& output) {
		preprocessor_state preprocessor = preprocessor_state();

		preprocessor.define_commands = {
			{"macro", { -1, true, cmd::define_macro }},
			{"multiblock", { -1, true, cmd::define_multiblock }},
			{"callblock", { -1, true, cmd::define_callblock }},
			{"include", { -1, false, cmd::include }},
			{"define", { 2, false, cmd::define }}
		};

		preprocessor.execute_commands = {
			{"use", { -1, true, cmd::use }},
			{"map", { 3, false, cmd::map }},
			{"if", { -1, true, cmd::if_ }},
			{"compare", { 3, true, cmd::compare }},
			{"range", { 3, true, cmd::range }},
			{"foreach", { 2, true, cmd::foreach }},
			{"indexof", { 2, false, cmd::indexof }}
		};

		process(preprocessor, input, output);
	}

	enum class SearchingState {
		searchingForKeyword = 0, gatheringArgs, gatheringBody, found
	};

	void process(preprocessor_state& preprocessor, std::istream& input, std::ostream& output) {
		std::stringstream post_definition;
		std::stringstream post_execution;

		parse(preprocessor, preprocessor.define_commands, input, post_definition);
		parse(preprocessor, preprocessor.execute_commands, post_definition, post_execution);
		post_execution << preprocessor.file_suffix;

		std::string temp_str = process_command_body(preprocessor, post_execution.str());
		output << temp_str;
	}

	std::string parse_stream(preprocessor_state& preprocessor, std::istream& input) {
		std::stringstream temp;
		std::stringstream output;

		parse(preprocessor, preprocessor.define_commands, input, temp);
		parse(preprocessor, preprocessor.execute_commands, temp, output);

		std::string output_str = output.str();
		if (output_str.size() < 1) return output_str;
		if (output_str.at(0) == '\t') output_str = output_str.substr(1);
		if (output_str.size() < 1) return output_str;
		if (output_str.at(output_str.size() - 1) == '\n') output_str = output_str.substr(0, output_str.size() - 1);
		return output_str;
	}

	std::string parse_string(preprocessor_state& preprocessor, std::string str) {
		if (str == "") return "";
		std::stringstream input(str);
		return parse_stream(preprocessor, input);
	}

	void parse(preprocessor_state& preprocessor, std::unordered_map<std::string, prp::command>& commandMap, std::istream& input, std::ostream& output) {
		input.seekg(0);
		std::string line;

		SearchingState state = SearchingState::searchingForKeyword;
		command* cmd = nullptr;
		std::vector<std::string> args;
		std::string body = "";
		int args_found = 0;
		bool do_newline = false;

		int scope = 0;
		while (std::getline(input, line)) {
			line = process_command_body(preprocessor, line);
			bool text_found = false;

	#if _DEBUG
			if (std::regex_match(line, std::regex(".*###@"))) {
				__debugbreak();
			}
	#endif

			std::vector<std::string> words = string_helper::split(line);
			if (words.size() > 0) {
				std::string word = words[0];
				if (std::regex_match(word, std::regex("###.*"))) {
					for (std::string word : words) {
						output << word << " ";
					}
					output << "\n";
					continue; // leave normal comments unprocessed
				} else if (std::regex_match(word, std::regex("##.*"))) {
					// leave double comments
				} else if (std::regex_match(word, std::regex("#.*"))) {
					continue; // remove normal comments
				}
			}		

			for (auto it = words.begin(); it != words.end(); it++) {
				std::string word = *it;
				if (word == "{") scope++;
				else if (word == "}") scope--;

				switch (state) {
				case SearchingState::searchingForKeyword: 
					cmd = get_command(word, commandMap);
					if (cmd != nullptr && (!cmd->has_body || scope == 0)) {
						if (cmd->arg_amount == 0) {
							if (cmd->has_body) state = SearchingState::gatheringBody;
							else state = SearchingState::found;
						} else {
							state = SearchingState::gatheringArgs;
						}
					}
					else {
						if (!text_found) output << "\t";
						text_found = true;
						output << word;
						if (it != words.end() - 1) output << " ";
					}
					break;
				case SearchingState::gatheringArgs:
					if (cmd->has_body && word == "{" && scope == 1) {
						state = SearchingState::gatheringBody;
					} else {
						args.push_back(word);
						args_found++;
						if (cmd->arg_amount < 0 && it == words.end() - 1) {
							state = SearchingState::found;
						} else if (cmd->arg_amount >= 0 && !cmd->has_body && args_found >= cmd->arg_amount) {
							if (cmd->has_body) state = SearchingState::gatheringBody;
							else state = SearchingState::found;
						}
					}
					break;
				case SearchingState::gatheringBody:
					if (word == "}" && scope == 0) {
						state = SearchingState::found;
					} else {
						body += word + " ";
					}
					break;
				}

				if (state == SearchingState::found) {
					if (args.size() == cmd->arg_amount || cmd->arg_amount < 0) {
						std::string s = cmd->function(preprocessor, *cmd, args, body);
						s = parse_string(preprocessor, s);
						if (s != "") {
							if (!text_found) output << "\t";
							text_found = true;
							output << s << " ";
						}

						state = SearchingState::searchingForKeyword;
						cmd = nullptr;
						args = std::vector<std::string>();
						body = "";
						args_found = 0;
					}
					else {
						output << string_helper::print_error("invalid argument amount!") << std::endl;
					}
					state = SearchingState::searchingForKeyword;
				}
			}
			if (body != "" && string_helper::trim(body) != "{" && state == SearchingState::gatheringBody) {
				body += "\n";
			}
			if (text_found || do_newline) {
				output << "\n";
				do_newline = text_found;
			}
		}
		std::flush(output);
	}
}