#include <iostream>
#include <fstream>
#include "Preprocessor.h"

int main(int argc, char* argv[]) {
	std::string input_path = "input.asc3";
	std::string output_path = "output.asc3";

	if (argc > 1) input_path = argv[1];
	if (argc > 2) output_path = argv[2];

	std::ifstream input(input_path);
	std::ofstream output(output_path);
	prp::process(input, output);
}

