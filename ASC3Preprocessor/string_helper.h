#pragma once
#include <vector>
#include <string>

namespace string_helper {
	std::vector<std::string> split(const std::string& str);
	std::vector<std::string> split(const std::string& str, char ch);
	std::string ltrim(const std::string& str);
	std::string rtrim(const std::string& str);
	std::string trim(const std::string& str);
	std::string join(std::vector<std::string> arguments);
	std::string join_file_path(std::vector<std::string> arguments);
	void stream_snapshot(std::stringstream& stream, const std::string& outputPath);
	std::string print_error(const std::string& str); // creates error str for preprocessor and prints the error in console
};