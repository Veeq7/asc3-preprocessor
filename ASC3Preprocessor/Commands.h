#pragma once
#include "preprocessor.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <memory>

namespace prp {
	typedef std::string (*command_function)
	   (preprocessor_state& preprocessor,
		command& cmd,
		const std::vector<std::string>& arguments,
		const std::string& body);

	struct command {
		int arg_amount = 0;
		bool has_body = false;
		command_function function;

		// macro / multiblock / callblock;
		std::string cmd = "";
		std::string block_body = "";
		std::vector<std::string> params = {};
	};

	bool replace_blocks(std::string& str, std::string block);
	bool replace_args(std::string& str, const std::vector<std::string>& params = {},
		const std::vector<std::string>& args = {}, std::string prefix = "\\b");

	bool replace_global_args(preprocessor_state& preprocessor, std::string& str, std::string prefix = "\\b");
	bool replace_expressions(preprocessor_state& preprocessor, std::string& str, const std::vector<std::string>& params = {},
		const std::vector<std::string>& args = {}, std::string prefix = "=");

	std::string process_command_body(preprocessor_state& preprocessor, std::string body, std::vector<std::string> params = {},
		std::vector<std::string> args = {}, std::string block = "");
	command* get_command(std::string keyword, std::unordered_map<std::string, command>& keywords);

	namespace cmd {
		std::string define_macro(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string define_multiblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string define_callblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string evaluate_macro(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string evaluate_multiblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string evaluate_callblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string evaluate_nameblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string include(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string define(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string map(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string use(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string if_(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string compare(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string range(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string foreach(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
		std::string indexof(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body);
	}
}