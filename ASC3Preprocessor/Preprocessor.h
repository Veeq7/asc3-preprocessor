#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <memory>

namespace prp {
	struct command;

	struct preprocessor_state {
		std::unordered_map<std::string, prp::command> define_commands;
		std::unordered_map<std::string, prp::command> execute_commands;
		std::unordered_set<std::string> used_blocks;
		std::unordered_set<std::string> include_guards;
		std::vector<std::pair<std::string, std::string>> globals;
		std::vector<std::pair<std::string, std::string>> maps;
		std::string file_suffix = "\n";
	};

	void process(std::istream& input, std::ostream& output);
	void process(preprocessor_state& preprocessor, std::istream& input, std::ostream& output);
	void parse(preprocessor_state& preprocessor, std::unordered_map<std::string, prp::command>& commandMap, std::istream& input, std::ostream& output);
	std::string parse_stream(preprocessor_state& preprocessor, std::istream& input);
	std::string parse_string(preprocessor_state& preprocessor, std::string str);
}