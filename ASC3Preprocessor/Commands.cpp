#include "commands.h"
#include <regex>
#include <iostream>
#include <sstream>
#include <fstream>
#include <numeric>
#include <algorithm>
#include "external/CMathParser.h"
#include "string_helper.h"

namespace prp {
	command* get_command(std::string keyword, std::unordered_map<std::string, command>& keywords) {
		if (keywords.find(keyword) != keywords.end()) {
			command* cmd = &keywords.at(keyword);
			return cmd;
		}
		return nullptr;
	}

	bool replace_blocks(std::string& str, std::string block) {
		if (block == "") return false;
		std::regex pattern("&block");
		if (std::regex_search(str, pattern)) {
			str = std::regex_replace(str, pattern, block);
			return true;
		}
		return false;
	}

	bool replace_args(std::string& str, const std::vector<std::string>& params, const std::vector<std::string>& args, std::string prefix) {
		bool found_arg = false;
		for (int i = 0; i < params.size(); i++) {
			std::regex pattern(prefix + params[i] + "\\b");

			if (std::regex_search(str, pattern)) {
				found_arg = true;
				str = std::regex_replace(str, pattern, args[i]);
			}
		}
		return found_arg;
	}

	bool replace_global_args(preprocessor_state& preprocessor, std::string& str, std::string prefix) {
		bool found_arg = false;
		for (std::pair<std::string, std::string> pair : preprocessor.globals) {
			std::regex pattern(prefix + pair.first + "\\b");

			if (std::regex_search(str, pattern)) {
				found_arg = true;
				str = std::regex_replace(str, pattern, pair.second);
			}
		}
		for (std::pair<std::string, std::string> p : preprocessor.maps) {
			std::regex pattern("@" + p.first + "\\b");

			if (std::regex_search(str, pattern)) {
				found_arg = true;
				str = std::regex_replace(str, pattern, p.second);
			}
		}
		return found_arg;
	}

	bool replace_expressions(preprocessor_state& preprocessor, std::string& str, const std::vector<std::string>& params, const std::vector<std::string>& args, std::string prefix) {
		bool found_arg = false;
		std::regex reg(prefix + "(.*?)([ \t\n;$])");
		std::sregex_iterator pos(str.begin(), str.end(), reg);
		std::sregex_iterator end;

		while (pos != end) {
			std::smatch matches = *pos;
			std::string match = matches[0].str();
			std::string expression = matches[1].str();
			std::string suffix = matches[2].str();
			replace_args(expression, params, args);
			replace_global_args(preprocessor, expression);

			CMathParser parser;
			int math_result = 0;
			std::string result = expression;
			if (parser.Calculate(expression.c_str(), &math_result) == CMathParser::ResultOk) {
				math_result = std::max(math_result, 0);
				result = std::to_string(math_result);
				found_arg = true;
				if (suffix == ";") suffix = "";
			}
			else {
				result = prefix + result;
#if _DEBUG
				std::cout << "Formula Iteration: " << parser.LastError()->Text << std::endl;
				std::cout << expression << std::endl;
#endif
			}

			result += suffix;
			std::string::const_iterator begin = matches[0].first;
			size_t offset = begin - str.begin();
			size_t match_size = match.length();
			if (begin != str.end()) {
				str.replace(begin, begin + match.length(), result);

				if (offset + match_size > str.size()) break;
				pos = std::sregex_iterator(str.begin() + offset + match_size, str.end(), reg);
				if (pos == end) break;
			}
			else {
				pos++;
			}
		}
		return found_arg;
	}

	std::string process_command_body(preprocessor_state& preprocessor, std::string body, std::vector<std::string> params, std::vector<std::string> args, std::string block) {
		replace_blocks(body, block);
		replace_args(body, params, args, "%");
		replace_global_args(preprocessor, body, "%");
		replace_expressions(preprocessor, body, params, args);

#ifdef _DEBUG
		std::cout << body << std::endl;
		std::cout << "==============================" << std::endl;
#endif

		return body;
	}

	std::string define_block(preprocessor_state& preprocessor, command& cmd,
			const std::vector<std::string>& arguments, const std::string& body, command_function function) {
		std::vector<std::string> macro_args;
		for (int i = 1; i < arguments.size(); i++) {
			macro_args.emplace_back(arguments[i]);
		}
		std::string cmd_name = arguments[0];
		preprocessor.define_commands.emplace("!" + cmd_name, command{ (int)macro_args.size(), false, function, cmd_name, body, arguments });
		if (function != cmd::evaluate_macro) {
			preprocessor.define_commands.emplace("*" + cmd_name, command{ 0, false, cmd::evaluate_nameblock, cmd_name });
		}
		return "";
	}

	std::string evaluate_block(preprocessor_state& preprocessor, command& cmd,
			const std::vector<std::string>& arguments, const std::string& body, std::string runKeyword) {
		std::string block_name = cmd.cmd;
		for (std::string arg : arguments) {
			std::string arg_copy = arg;
			char chars_to_filter[] = { ':', ',', '.', '!' };
			for (char c : chars_to_filter) {
				std::replace(arg_copy.begin(), arg_copy.end(), c, '_');
			}
			block_name += "_" + arg_copy;
		}
		if (preprocessor.used_blocks.find(block_name) == preprocessor.used_blocks.end()) {
			preprocessor.used_blocks.insert(block_name);
			preprocessor.file_suffix += "\n\t" + parse_string(preprocessor, process_command_body(preprocessor, ":" + block_name + "\n" + cmd.block_body + "\n", cmd.params, arguments, block_name));
		}
		if (runKeyword != "") runKeyword += " ";
		return runKeyword + block_name;
	}

	namespace cmd {
		std::string define_macro(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			return define_block(preprocessor, cmd, arguments, body, evaluate_macro);
		}

		std::string define_multiblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			return define_block(preprocessor, cmd, arguments, body, evaluate_multiblock);
		}

		std::string define_callblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			return define_block(preprocessor, cmd, arguments, body, evaluate_callblock);
		}

		std::string evaluate_macro(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			return parse_string(preprocessor, process_command_body(preprocessor, cmd.block_body, cmd.params, arguments, ""));
		}

		std::string evaluate_multiblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			return evaluate_block(preprocessor, cmd, arguments, body, "multirun");
		}

		std::string evaluate_callblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			return evaluate_block(preprocessor, cmd, arguments, body, "call");
		}

		std::string evaluate_nameblock(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			return evaluate_block(preprocessor, cmd, arguments, body, "");
		}

		std::string include(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			std::string file_name = string_helper::join_file_path(arguments);

			if (preprocessor.include_guards.find(file_name) == preprocessor.include_guards.end()) {
				preprocessor.include_guards.insert(file_name);
			}
			else {
				return "";
			}

			std::ifstream input(file_name);
			std::string result = parse_stream(preprocessor, input);

			return result;
		}

		std::string define(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			preprocessor.globals.push_back(std::make_pair(arguments[0], arguments[1]));
			return "";
		}

		std::string map(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			std::string key = arguments[0] + "\\$" + arguments[1];
			auto it = std::find_if(preprocessor.maps.begin(), preprocessor.maps.end(), [&key](std::pair<std::string, std::string>& pair) {
				return pair.first == key;
				});
			if (it != preprocessor.maps.end()) { // overwrite
				it->second = arguments[2];
			}
			else { // add
				preprocessor.maps.push_back(std::make_pair(key, arguments[2]));
			}
			return "";
		}

		std::string use(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			std::vector<std::string> params;
			std::vector<std::string> args;
			for (std::string s : arguments) {
				std::vector<std::string> pair = string_helper::split(s, ':');
				if (pair.size() != 2) {
					return string_helper::print_error("Invalid declaration in use block: `" + s + "`!");
				}
				params.push_back(pair[0]);
				args.push_back(pair[1]);
			}
			return process_command_body(preprocessor, body, params, args, "");
		}

		std::string if_(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			CMathParser parser;

			int math_result = 0;
			if (parser.Calculate(string_helper::join(arguments).c_str(), &math_result) != CMathParser::ResultOk) {
#if _DEBUG
				printf("If Formula Iteration: %s\n", parser.LastError()->Text);
#endif
			}

			return math_result ? body : "";
		}

		std::string compare(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			bool is_true = false;
			const std::string& str1 = arguments[0];
			const std::string& str2 = arguments[2];
			const std::string& op = arguments[1];

			if (op == "==") {
				is_true = str1 == str2;
			}
			else if (op == "!=") {
				is_true = str1 != str2;
			}
			else {
				return string_helper::print_error("invalid operator");
			}

			return is_true ? body : "";
		}

		std::string range(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			std::string ret;
			for (int i = std::stoi(arguments[1]); i <= std::stoi(arguments[2]); i++) {
				std::vector<std::string> params;
				std::vector<std::string> args;
				params.push_back(arguments[0]);
				args.push_back(std::to_string(i));
				ret += process_command_body(preprocessor, body, params, args, "");
			}
			return ret;
		}

		std::string foreach(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			std::string ret = "";
			if (arguments.size() % 2 != 0 || arguments.size() < 2) {
				return string_helper::print_error("foreach with incorrect array binding!");
			}
			std::vector<std::string> keys = string_helper::split(arguments[0], ':');
			std::vector<std::string> values = string_helper::split(arguments[1], ',');
			int bindingSize = keys.size();
			int arraySize = values.size();
			for (int i = 0; i < arraySize; i++) {
				std::vector<std::string> params;
				std::vector<std::string> args;
				std::string body_copy = body;
				std::regex reg("&index");
				if (std::regex_search(body_copy, reg)) {
					body_copy = std::regex_replace(body_copy, reg, std::to_string(i));
				}
				reg = std::regex("&size");
				if (std::regex_search(body_copy, reg)) {
					body_copy = std::regex_replace(body_copy, reg, std::to_string(arraySize));
				}
				for (int j = 0; j < bindingSize; j++) {
					params.push_back(keys[j]);
					std::vector<std::string> vec = string_helper::split(values[i], ':');
					if (vec.size() < bindingSize) {
						return string_helper::print_error("foreach array sizes aren't matching!");
					}
					args.push_back(vec[j]);
				}
				ret += process_command_body(preprocessor, body_copy, params, args, "");
			}
			return ret;
		}

		std::string indexof(preprocessor_state& preprocessor, command& cmd, const std::vector<std::string>& arguments, const std::string& body) {
			std::string ret = "";
			try {
				std::vector<std::string> keys = string_helper::split(arguments[1], '$');
				size_t index = std::stoi(keys[0]);
				std::vector<std::string> values = string_helper::split(arguments[0], ',');
				if (index >= values.size()) {
					return string_helper::print_error("unreachable index");
				}
				if (keys.size() == 1) {
					ret = values[index];
				}
				else if (keys.size() == 2) {
					size_t key = std::stoi(keys[1]);
					std::vector<std::string> bindings = string_helper::split(values[index], ':');
					if (key >= bindings.size()) {
						return string_helper::print_error("unreachable key");
					}
					ret = bindings[key];
				}
				else {
					return string_helper::print_error("incorrect key parameter");
				}
			}
			catch (std::exception& e) {
				return string_helper::print_error("invalid index or key: " + std::string(e.what()));
			}

			return ret;
		}
	}
}